<?php
define('GRPHIMGP_SHORTNAME', 'grph_imgp');  
define('GRPHIMGP_PAGE_BASENAME', 'grph_imgp_settings');

add_action( 'admin_menu', 'grph_imgp_add_menu');  
add_action( 'admin_init', 'grph_imgp_register_settings');
if ( is_admin() ) :
function grph_imgp_get_settings() { 
	$output = array();   
 	$output['grph_imgp_option_name']	= 'grph_imgp_options';   
	$output['grph_imgp_page_title']	= 'Graph Image Save'; 
	return $output; 
} 
function grph_imgp_settings_scripts(){
	
    wp_enqueue_style('grph_imgp_settings_css', plugins_url( '/css/grph_imgp_settings.css', __FILE__));
    wp_enqueue_script('jquery');
    /*wp_enqueue_style('tabs_accordion_css', plugins_url('/css/tabs-accordion.css', __FILE__));
    
    wp_enqueue_script('grph_imgp_settings_js',
		plugins_url( '/js/grph_imgp_settings.js' , __FILE__ ), array('jquery'));
	wp_enqueue_script('jquery_tools_min_js',
		plugins_url('/js/jquery.tools.min.js', __FILE__ ),  array('jquery'));
	*/
	
} 
function grph_imgp_register_settings(){  
	$settings_output = grph_imgp_get_settings();
	$grph_imgp_option_name = $settings_output['grph_imgp_option_name'];  
	register_setting($grph_imgp_option_name, $grph_imgp_option_name);	 
}
function grph_imgp_add_menu(){  
	 $grph_imgp_settings_page = add_menu_page('Graph Image Save','Graph Image','administrator',GRPHIMGP_PAGE_BASENAME,'grph_imgp_settings_page_fn',plugins_url('grph-image-poster/icon.png'));
 	 $grph_imgp_hook = add_submenu_page( GRPHIMGP_PAGE_BASENAME, 'Index Web Url', 'Index Web Url', 'manage_options', GRPHIMGP_PAGE_BASENAME.'_index_web','grph_imgp_settings_page_index_web');
 	 $grph_imgp_hook_2 = add_submenu_page( GRPHIMGP_PAGE_BASENAME, 'Index Paste HTML', 'Index Paste HTML', 'manage_options', GRPHIMGP_PAGE_BASENAME.'_index_paste_html','grph_imgp_settings_page_index_paste_html');
 	 
	 add_action( 'load-'. $grph_imgp_settings_page, 'grph_imgp_settings_scripts' );
	 add_action( 'load-'. $grph_imgp_hook, 'grph_imgp_settings_scripts' );
	 add_action( 'load-'. $grph_imgp_hook_2, 'grph_imgp_settings_scripts' );	
}
function grph_imgp_settings_page_fn() {
	?>
	<div class="wrap"> 
		<h2>Graph and save image from url link</h2>  
		    <!--before  container-->
		    <div class="metabox-holder" id="dashboard-widgets">
		        <div style="width:80%;" class="postbox-container">
		            <div style="min-height:1px;" class="meta-box-sortables ui-sortable" id="normal-sortables">
		                <div class="postbox" id="dashboard_right_now">
		                    <h3 class="hndle">
		                        <span>Graph and save image</span>
		                    </h3>
		                    <div>
		                    <form method="post" action="" id="submitAjax" name="myform-reset"> 
		                    <table class="form-table">
						        <tr valign="top">
						        <th scope="row"><b>URL link Image :</b></th>
						        <td>
						        	<textarea rows="8" cols="120" name="url" id="url"></textarea>
						        	<br>ex: http://genthemes.net/photo/amXg7rV_460s.jpg
						        	<label><span id="required"></span></label>
						        </td>
						        </tr>
						        <!--  
						        <tr valign="top">
						        <th scope="row">Name</th>
						        <td><input type="text" name="newfilename"></td>
						        </tr>-->
						        <tr valign="top">
						        <th scope="row"></th>
						        <td>
						        <input name="submitted" value="Save Options" type="submit" class="button-primary" id="subscribe_button">
						        <input value="Reset" type="submit" class="button-primary" id="reset_button" onclick="resetForm()">
						        </td>
						        </tr>  
						    </table> 
			                </form>     
		                    </div> 
							 <table class="form-table">
						        <tr valign="top"> 
						        <th scope="row"></th> 
						        </tr>
						        <tr valign="top"> 
						        <th scope="row"> 
								
						        <script type="text/javascript"> 
						        <?php bloginfo('url'); ?>/wp-admin/admin-ajax.php
						        <?php 
						        $dir = WP_PLUGIN_URL.'/'.str_replace(basename( __FILE__),"",plugin_basename(__FILE__)); 
						        ?> 
						        jQuery('#subscribe_button').click(loadSubmit);  
						        function loadSubmit(){
						        	var url = jQuery('#url').val();
						        	var arrayOfLines = jQuery('#url').val().split('\n');
						        	var numTotal = arrayOfLines.length;
						        	if (url == '') { 
						   				//jQuery('#info_message').html('<div class="ops">Oops! Field can not be empty</div>'); 
						   				jQuery('#url').addClass('error-input');
						   				jQuery('#required').html('<div class="ops">Oops! Field can not be empty</div>');
 						   			} 
						   			else {
						   				jQuery('#required').html('');
						   				jQuery('#url').removeClass('error-input');
										var i; 
										var resultHTML='<table class="widefat fixed"><thead><tr><th align="left"><strong>URL Link <span class="result"></span></strong></th><th align="left"><strong>Status <span id="total"></span></strong></th></tr></thead>';
							        	for(i=0; i<arrayOfLines.length; i++){ 
							        		resultHTML+='<td style="padding:10px">'+arrayOfLines[i]+'</td>';
							        		resultHTML+='<td class="act'+i+'" style="padding:10px">'+cekSubmit(arrayOfLines[i],i)+'</td></tr>';
							        		
							        	} 
						        		//resultHTML+='<tr><td  style="padding:10px" class="result"></td></tr></table>';
						        		jQuery('#info_message').html(resultHTML);
						        		recordSubmit(i);
						   			}
						   			return false;  
							    }
 
						        function cekSubmit(url,num){  
						        	var result; 
						        	result = "<span class='blink_me'><i>loading..</i></span>"; 
						        	jQuery('.act'+num).html(result); 
					   				jQuery.ajax({ 
					   					url: "<?php echo $dir;?>/class/core.php",
					   					type: 'POST',
					   					data: { url: url}, 
					   					success: function(resp){ 
					   						jQuery('.act'+num).html(resp);
						   					var str = jQuery('.act'+num).text(); 
						   					if(str == 'is not a valid image'){ 
						   						 
						   					}
						   					if(str == 'The file cannot be saved'){ 
						   					}
											if(str == 'Success saved'){ 
												jQuery('.act'+num).html('<span style="color:green">Success Saved</span><input type="hidden" value="1" class="sum[]" name="sum[]">');
												recordSuccess();
						   					} 
					   					}
					   				}); 
					   				return result;  
							    } 
						        function resetForm()
						        {
						            document.forms["myform-reset"].reset(); 
						        }
						        function recordSubmit(num) {
						        	jQuery('.result').html('('+num+')');  
						        	return false;
						        } 
						        function recordSuccess() { 
						        	var items = document.getElementsByName('sum[]');
						            var itemCount = items.length;
						            var total = 0;
						            for(var i = 0; i < itemCount; i++)
						            {
						                total = total +  parseInt(items[i].value);
						                 
						            }
						            document.getElementById('total').innerHTML=' ('+total+') Success Saved ';
						        } 
						        function basename(path) {
						            return path.replace(/\\/g,'/').replace( /.*\//, '' );
						        } 
                                </script> 
						        <div id="info_message"></div> 
						        <div id="progress"></div> 
						        </th> 
						        </tr>   
						    </table> 
						</div>
		            	</div>
		             </div>
		    </div> 
		 
		   <!-- / start container--> 
<?php
}  
function grph_imgp_settings_page_index_web() {
?>
		<div class="wrap"> 
		<h2>Graph and save image from URL Web</h2>  
		    <!--before  container-->
		    <div class="metabox-holder" id="dashboard-widgets">
		        <div style="width:80%;" class="postbox-container">
		            <div style="min-height:1px;" class="meta-box-sortables ui-sortable" id="normal-sortables">
		                <div class="postbox" id="dashboard_right_now">
		                    <h3 class="hndle">
		                        <span>Graph and save image</span>
		                    </h3>
		                    <div>
		                    <form method="post" action="" id="submitAjaxIndex" name="myform-resetIndex"> 
		                    <table class="form-table">
						        <tr valign="top">
						        <th scope="row"><b>URL Web :</b></th>
						        <td>
						        	<input type="text" name="url" id="url" size="70%">
						        	<br>ex: http://genthemes.net/
						        	<br><label><input type="checkbox" value="on" name="autosave" id="autosave">&nbsp;Auto Save Media</label>
						        	<label><span id="required"></span></label>
						        </td> 
						        </tr>
						        <tr valign="top">
						        <th scope="row"><b>Max Execution Time :</b></th>
						        <td>
						        	<input type="text" value="1000" name="max_execution_time" id="max_execution_time">
						        	<label>seconds</label>
						        </td> 
						        </tr>
						        <tr valign="top">
						        <th scope="row"></th>
						        <td>
						        <input name="submitted" value="Save Options" type="submit" class="button-primary" id="subscribe_button_index">&nbsp;<span class="resultIndex"></span>
 						         
 						        </td>
						        </tr>  
						    </table> 
			                </form>     
		                    </div> 
							 <table class="form-table">
						        <tr valign="top"> 
						        <th scope="row"></th> 
						        </tr>
						        <tr valign="top"> 
						        <th scope="row"> 
								
						        <script type="text/javascript"> 
						        <?php bloginfo('url'); ?>/wp-admin/admin-ajax.php
						        <?php 
						        $dir = WP_PLUGIN_URL.'/'.str_replace(basename( __FILE__),"",plugin_basename(__FILE__)); 
						        ?> 
						        jQuery('#subscribe_button_index').click(ajaxSubmitIndex);   
						        function ajaxSubmitIndex(){ 
						        	var url = jQuery('#url').val();
						        	var max_execution_time = jQuery('#max_execution_time').val(); 
						        	if (url == '') { 
						   				//jQuery('#info_message').html('<div class="ops">Oops! Field can not be empty</div>'); 
						   				jQuery('#url').addClass('error-input2');
						   				jQuery('#required').html('<div class="ops">Oops! Field can not be empty</div>');
 						   			} else{
						        	if(jQuery('#autosave').is(':checked')){
						        		var autosave = jQuery('#autosave').val();
						        	} else { var autosave = ""; } 
						        	var result; 
						        	result = "<span class='blink_me'><i>loading..</i></span>"; 
						        	jQuery('.resultIndex').html(result); 
						        	jQuery('#required').html('');
			   						jQuery('#url').removeClass('error-input2');  
					   				jQuery.ajax({ 
					   					url: "<?php echo $dir;?>/class/poster.php",
					   					type: 'POST',
					   					data: { url: url, autosave: autosave, max_execution_time: max_execution_time}, 
					   					success: function(resp){  
					   						jQuery('#info_message_index').html(resp);
					   						jQuery('.resultIndex').html('');  
					   					}
					   				}); }
					   				return false;  
							    }  
						         
						        jQuery('.save').live( "click", function() {
						        	var urlsingle = jQuery(this).attr("id"); 
						        	var num = jQuery(this).attr("data");  
						        	jQuery('.loading'+num).html("<span class='blink_me'><i>loading..</i></span>");
						        	jQuery('.hidden'+num).hide();
						        	jQuery.ajax({ 
					   					url: "<?php echo $dir;?>/class/poster.php",
					   					type: 'POST',
					   					data: { urlsingle: urlsingle}, 
					   					success: function(resp){   
					   						jQuery('.resultIndexSingle'+num).html('<span style="color:green">Success Saved</span>'); 
					   						jQuery('.hidden'+num).hide();
					   						jQuery('.loading'+num).html('');
					   					}
					   				});
					   				return false;
						        });
                                </script> 
						        <div id="info_message_index"></div>  
						        </th> 
						        </tr>   
						    </table> 
						</div>
		            	</div>
		             </div>
		    </div> 
		 
		   <!-- / start container--> 	
	
<?php
}
function grph_imgp_settings_page_index_paste_html() {
?>
		<div class="wrap"> 
		<h2>Graph and save image from paste code html</h2>  
		    <!--before  container-->
		    <div class="metabox-holder" id="dashboard-widgets">
		        <div style="width:80%;" class="postbox-container">
		            <div style="min-height:1px;" class="meta-box-sortables ui-sortable" id="normal-sortables">
		                <div class="postbox" id="dashboard_right_now">
		                    <h3 class="hndle">
		                        <span>Graph and save image</span>
		                    </h3>
		                    <div>
		                    <form method="post" action="" id="submitAjaxIndex" name="myform-resetIndex"> 
		                    <table class="form-table">
						        <tr valign="top">
						        <th scope="row"><b>Paste code HTML :</b></th>
						        <td>
						        	<textarea rows="8" cols="120" name="content" id="content"></textarea> 
						        	<br> ex: view source http://.., copy code html and paste
						        	<br><label><input type="checkbox" value="on" name="autosave" id="autosave">&nbsp;Auto Save Media</label>
						        	<label><span id="required"></span></label>
						        </td> 
						        </tr>
						        <tr valign="top">
						        <th scope="row"><b>Max Execution Time :</b></th>
						        <td>
						        	<input type="text" value="1000" name="max_execution_time" id="max_execution_time">
						        	<label>seconds</label>
						        </td> 
						        </tr>
						        <tr valign="top">
						        <th scope="row"></th>
						        <td>
						        <input name="submitted" value="Save Options" type="submit" class="button-primary" id="subscribe_button_index_paste">&nbsp;<span class="resultIndex"></span>
 						         
 						        </td>
						        </tr>  
						    </table> 
			                </form>     
		                    </div> 
							 <table class="form-table">
						        <tr valign="top"> 
						        <th scope="row"></th> 
						        </tr>
						        <tr valign="top"> 
						        <th scope="row"> 
								
						        <script type="text/javascript"> 
						        <?php bloginfo('url'); ?>/wp-admin/admin-ajax.php
						        <?php 
						        $dir = WP_PLUGIN_URL.'/'.str_replace(basename( __FILE__),"",plugin_basename(__FILE__)); 
						        ?> 
						        jQuery('#subscribe_button_index_paste').click(ajaxSubmitIndexPaste);   
						        function ajaxSubmitIndexPaste(){ 
						        	var content = jQuery('#content').val();  
						        	var max_execution_time = jQuery('#max_execution_time').val(); 
						        	if (content == '') { 
						   				//jQuery('#info_message').html('<div class="ops">Oops! Field can not be empty</div>'); 
						   				jQuery('#content').addClass('error-input');
						   				jQuery('#required').html('<div class="ops">Oops! Field can not be empty</div>');
 						   			} else{
						        	if(jQuery('#autosave').is(':checked')){
						        		var autosave = jQuery('#autosave').val();
						        	} else { var autosave = ""; } 
						        	
						        	var result; 
						        	result = "<span class='blink_me'><i>loading..</i></span>"; 
						        	jQuery('.resultIndex').html(result); 
						        	jQuery('#required').html('');
			   						jQuery('#content').removeClass('error-input');  
					   				jQuery.ajax({ 
					   					url: "<?php echo $dir;?>/class/poster.php",
					   					type: 'POST',
					   					data: { content: content, autosave: autosave, max_execution_time: max_execution_time}, 
					   					success: function(resp){  
					   						jQuery('#info_message_index').html(resp);
					   						jQuery('.resultIndex').html('');  
					   					}
					   				}); }
					   				return false;   
							    }  
						        jQuery('.save').live( "click", function() {
						        	var urlsingle = jQuery(this).attr("id"); 
						        	var num = jQuery(this).attr("data");  
						        	jQuery('.loading'+num).html("<span class='blink_me'><i>loading..</i></span>");
						        	jQuery('.hidden'+num).hide();
						        	jQuery.ajax({ 
					   					url: "<?php echo $dir;?>/class/poster.php",
					   					type: 'POST',
					   					data: { urlsingle: urlsingle}, 
					   					success: function(resp){   
					   						jQuery('.resultIndexSingle'+num).html('<span style="color:green">Success Saved</span>'); 
					   						jQuery('.hidden'+num).hide();
					   						jQuery('.loading'+num).html('');
					   					}
					   				});
					   				return false;
						        });
						          
                                </script> 
						        <div id="info_message_index"></div>  
						        </th> 
						        </tr>   
						    </table> 
						</div>
		            	</div>
		             </div>
		    </div> 
		 
		   <!-- / start container--> 	
	
<?php
}
endif;
?>