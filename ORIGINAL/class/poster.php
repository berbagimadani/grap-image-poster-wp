<?php
/*
Plugin Name: Graph and Save Image From Web
Description: Plugins Graph and Save Image From URL, HTML, link web
Version: 1.0.1
Author: Ade Iskandar
Author URI: http://genthemes.net / berbagimadani@gmail.com
*/
include("simple_html_dom.php");
class poster {    
    	function getContent($url,$autosave){
    			if($this->Visit($url)){ 
				$html = file_get_html($url);  
				if (method_exists($html,"find")) { 
					 if ($html->find('html')) { 
						$i=1; 
						echo '<table class="widefat fixed"><thead><tr><th style="width:20px"><strong>No</strong></th><th align="left"><strong>Image</strong></th><th align="left"><strong>Status</strong></th></tr></thead>';
						foreach($html->find('img') as $e){ 
							 $v="";
							 if(!empty($autosave))	{ 
							 $v = $this->each($e->src);
							 }
							 else{
							 $v = '<span class="loading'.$i.'"></span><span class="resultIndexSingle'.$i.'"></span><input type="hidden" name="urlsingle" value="'.$e->src.'" id="urlsingle"><input value="Save Media" type="submit" class="save hidden'.$i.' button-primary" id="'.$e->src.'" data="'.$i.'">';
							 }
							 echo "<tr><td style='padding:10px'>$i</td><td><img src='$e->src' width='200'></td><td>$v</td></tr>";  
							  
							 $i++;
						} 
						echo "</table>"; 
					 }
				} }
				else { echo "Website DOWN, Please Cek URL web"; }
		    return false; 
	    }
	    function getContentHtml($url,$autosave){  
	    	/*$doc = new DOMDocument();
	    	libxml_use_internal_errors(true);
			$doc->loadHTML($url);
			libxml_clear_errors(); 
			$html2 = $doc->saveHTML();*/
			
	    	$tes = new simple_html_dom();  
			$tes->load($url);   
			$ret = $tes->find('img'); 
				$i = 1; 
				echo '<table class="widefat fixed"><thead><tr><th><strong>Image</strong></th><th style="width:40px"><strong>Status</strong></th></tr></thead>';
				foreach($ret as $key=>$e){ 
					$v="";
					if(!empty($autosave))	{ 
						$v = $this->each(stripslashes(str_replace('"','',$e->src)));
					}else{
						$v = '<span class="loading'.$i.'"></span><span class="resultIndexSingle'.$i.'"></span><input type="hidden" name="urlsingle" value="'.stripslashes(str_replace('"','',$e->src)).'" id="urlsingle"><input value="Save Media" type="submit" class="save hidden'.$i.' button-primary" id="'.stripslashes(str_replace('"','',$e->src)).'" data="'.$i.'">';
					} 
					echo "<tr><td style='padding:10px'><img src=".stripslashes(str_replace('"','',$e->src))."></td><td style='padding:10px'>$v</td></tr>";
				$i++;
				}
				echo "</table>"; 
		    return false; 
	    } 
		function fetch_image($url) {
			if ( function_exists("curl_init") ) {
				return $this->curl_fetch_image($url);
			} elseif ( ini_get("allow_url_fopen") ) {
				return $this->fopen_fetch_image($url);
			}
		}
		function curl_fetch_image($url) {
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$image = curl_exec($ch);
			curl_close($ch);
			return $image;
		}
		function fopen_fetch_image($url) {
			$image = file_get_contents($url, false, $context);
			return $image;
		}
		function each($url){  
			return $this->grab_image($url); 
		}
		function grab_image($url){ 
			$uploads = wp_upload_dir();
			$ext = pathinfo( basename($url) , PATHINFO_EXTENSION);
			$newfilename = isset($_POST['newfilename']) ? isset($_POST['newfilename']) . "." . $ext : utf8_decode(basename($url));
			$filename = wp_unique_filename( $uploads['path'], $newfilename, $unique_filename_callback = null );
			$wp_filetype = wp_check_filetype($filename, null );
			$fullpathfilename = $uploads['path'] . "/" . $filename;   
			
			try {
					if ( !substr_count($wp_filetype['type'], "image") ) {
						//throw new Exception( basename($url) . ' is not a valid image. ' . $wp_filetype['type']  . '' );
						throw new Exception('<span style="color:red">is not a valid image</span>' . $wp_filetype['type']  . '' );
						 
					}
				
					$image_string = $this->fetch_image($url);
					$fileSaved = file_put_contents($uploads['path'] . "/" . $filename, $image_string);
					if ( !$fileSaved ) {
						@unlink($fullpathfilename);
						throw new Exception('<span style="color:red">The file cannot be saved</span>');
					}
					else{
						
						$attachment = array(
							 'post_mime_type' => $wp_filetype['type'],
							 'post_title' => preg_replace('/\.[^.]+$/', '', $filename),
							 'post_content' => '',
							 'post_status' => 'inherit',
							 'guid' => $uploads['url'] . "/" . $filename
						);
						$attach_id = wp_insert_attachment( $attachment, $fullpathfilename);
						if ( !$attach_id ) {
							throw new Exception("Failed to save record into database.");
						}
						require_once(ABSPATH . "wp-admin" . '/includes/image.php');
						$attach_data = wp_generate_attachment_metadata( $attach_id, $fullpathfilename );
						wp_update_attachment_metadata( $attach_id,  $attach_data ); 
					 	//echo '<div id="message-" class="sucess-"><img src="'.wp_get_attachment_thumb_url($attach_id).'"></div>';
						return '<span style="color:green">Success saved</span>';
					}
			} catch (Exception $e) {
					return  '<span style="color:red">' . $e->getMessage() . '</span>';
			}
			
			return false;
		}
		function Visit($url){
		       $agent = "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)";$ch=curl_init();
		       curl_setopt ($ch, CURLOPT_URL,$url );
		       curl_setopt($ch, CURLOPT_USERAGENT, $agent);
		       curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
		       curl_setopt ($ch,CURLOPT_VERBOSE,false);
		       curl_setopt($ch, CURLOPT_TIMEOUT, 5);
		       curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, FALSE);
		       curl_setopt($ch,CURLOPT_SSLVERSION,3);
		       curl_setopt($ch,CURLOPT_SSL_VERIFYHOST, FALSE);
		       $page=curl_exec($ch);
		       //echo curl_error($ch);
		       $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		       curl_close($ch);
		       if($httpcode>=200 && $httpcode<300) return true;
		       else return false;
		}
		/*
		function curl_get_file_size( $url ) {
		  // Assume failure.
		  $result = -1;
		  //$headers[] = 'Accept: image/gif, image/x-bitmap, image/jpeg, image/pjpeg';
          //$headers[] = 'Connection: Keep-Alive';
          //$headers[] = 'Content-type: application/x-www-form-urlencoded;charset=UTF-8';
          
		  $curl = curl_init( $url );
		
		  // Issue a HEAD request and follow any redirects.
		  //curl_setopt( $curl, CURLOPT_HTTPHEADER, $headers);
		  curl_setopt( $curl, CURLOPT_NOBODY, true );
		  curl_setopt( $curl, CURLOPT_HEADER, true );
		  curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
		  curl_setopt( $curl, CURLOPT_FOLLOWLOCATION, true );
		  //curl_setopt( $curl, CURLOPT_USERAGENT, get_user_agent_string() );
		  
		  $data = curl_exec( $curl );
		  curl_close( $curl );
		
		  if( $data ) {
		    $content_length = "unknown";
		    $status = "unknown";
		
		    if( preg_match( "/^HTTP\/1\.[01] (\d\d\d)/", $data, $matches ) ) {
		      $status = (int)$matches[1];
		    }
		
		    if( preg_match( "/Content-Length: (\d+)/", $data, $matches ) ) {
		      $content_length = (int)$matches[1];
		    } 
		    // http://en.wikipedia.org/wiki/List_of_HTTP_status_codes
		    if( $status == 200 || ($status > 300 && $status <= 308) ) {
		      $result = $content_length;
		    }
		  } 
		  return $result;
		}
		function filesize2bytes($bytes) { 
		    if ($bytes >= 1073741824)
	        {
	            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
	        }
	        elseif ($bytes >= 1048576)
	        {
	            $bytes = number_format($bytes / 1048576, 2) . ' MB';
	        }
	        elseif ($bytes >= 1024)
	        {
	            $bytes = number_format($bytes / 1024, 2) . ' KB';
	        }
	        elseif ($bytes > 1)
	        {
	            $bytes = $bytes . ' bytes';
	        }
	        elseif ($bytes == 1)
	        {
	            $bytes = $bytes . ' byte';
	        }
	        else
	        {
	            $bytes = '0 bytes';
	        } 
	        return $bytes;
		}  */
} 
		ini_set('max_execution_time', $_POST["max_execution_time"]);
		ini_set("memory_limit","120M");
		$plugin_name = 'grph-image-poster';
		$oldURL = dirname(__FILE__);
		$newURL = str_replace(DIRECTORY_SEPARATOR . 'wp-content' . DIRECTORY_SEPARATOR . 'plugins' . DIRECTORY_SEPARATOR . $plugin_name, '', $oldURL);
		include($newURL . DIRECTORY_SEPARATOR . '../wp-load.php');
		$m = new poster();
		if(isset($_POST["url"])){
			$url = $_POST["url"];
			$autosave =  $_POST["autosave"]; 
			$m->getContent($url,$autosave);
		} 
		if(isset($_POST["urlsingle"])){
			$url = $_POST["urlsingle"]; 
			$m->each($url);
		} 
		if(isset($_POST["content"])){
			$content = $_POST["content"]; 
			$autosave =  $_POST["autosave"]; 
			$m->getContentHtml($content,$autosave);
		} 
?>