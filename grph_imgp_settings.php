<?php
/*
Plugin Name: Graph and Save Image From Web
Description: Plugins Graph and Save Image From URL, HTML, link web
Version: 1.0.1
Author: Ade Iskandar
Author URI: http://genthemes.net / berbagimadani@gmail.com
*/
?>
<?php
define('GRPHIMGP_FOLDER', dirname(plugin_basename(__FILE__))); 
define('GRPHIMGP_PLUGIN_NAME', plugin_basename(__FILE__) );  
if (is_admin()){
	require_once( plugin_dir_path( __FILE__ ). 'grph_imgp_options.php'); 
}

register_activation_hook(__FILE__,'grph_imgp_install');
function grph_imgp_install(){
	//update_option('genthemes_version', '1.0.1');
	  add_option("grph_imgp_options", '', '', 'yes'); 
}

register_deactivation_hook( __FILE__, 'grph_imgp_remove' );
function grph_imgp_remove() { 
	delete_option('grph_imgp_options');
} 
  
function grph_imgp_link($links) {
  $settings_link = '<a href="themes.php?page=grph_imgp_settings">Settings</a>';
  array_unshift($links, $settings_link);
  return $links;
}

$plugin = plugin_basename(__FILE__);
add_filter("plugin_action_links_$plugin", 'grph_imgp_link' );
?>